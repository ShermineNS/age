package com.age.agebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AgebackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(AgebackendApplication.class, args);
	}

}
