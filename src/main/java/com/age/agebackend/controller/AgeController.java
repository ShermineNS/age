package com.age.agebackend.controller;

import com.age.agebackend.dto.AgeReqDTO;
import com.age.agebackend.dto.AgeResDTO;
import com.age.agebackend.service.AgeService;
import com.age.agebackend.utils.AppResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class AgeController {

    @Autowired
    private AgeService ageService;

    @GetMapping("/test")
    public String testApi() {
        return "Success";
    }
    @PostMapping("/age/add")
    public AppResponse<AgeResDTO> saveAge(@RequestBody final AgeReqDTO ageReqDTO) {
        AgeResDTO ageResDTO = ageService.saveAge(ageReqDTO);
        if (ageResDTO != null) {
            return AppResponse.<AgeResDTO>builder()
                    .data(ageResDTO)
                    .success(true)
                    .message("Successfully Added")
                    .build();

        } else {
            return AppResponse.<AgeResDTO>builder()
                    .success(false)
                    .message("Something Went wrong")
                    .build();

        }
    }
}
