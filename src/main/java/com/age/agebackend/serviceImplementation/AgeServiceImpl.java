package com.age.agebackend.serviceImplementation;


import com.age.agebackend.dto.AgeReqDTO;
import com.age.agebackend.dto.AgeResDTO;
import com.age.agebackend.entity.Age;
import com.age.agebackend.repository.AgeRepository;
import com.age.agebackend.service.AgeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class AgeServiceImpl implements AgeService {

    @Autowired
    private AgeRepository ageRepository;

    @Override
    public AgeResDTO saveAge(AgeReqDTO ageReqDTO) {


        Age age = new Age();
        age.setFirstName(ageReqDTO.getFirstName());
        age.setLastName(ageReqDTO.getLastName());
        age.setDob(ageReqDTO.getDob());
        age.setPresentAge(ageReqDTO.getPresentAge());
        ageRepository.save(age);


        return this.saveProcessAge(age);
    }

    private AgeResDTO saveProcessAge(final Age age) {

        AgeResDTO ageResDTO = new AgeResDTO();

        ageResDTO.setFirstName(age.getFirstName());
        ageResDTO.setLastName(age.getLastName());
        ageResDTO.setDob(age.getDob());
        ageResDTO.setPresentAge(age.getPresentAge());

        return ageResDTO;
    }
}

