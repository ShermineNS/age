package com.age.agebackend.dto;

import com.sun.istack.NotNull;
import lombok.Data;

import java.util.Date;

@Data
public class AgeReqDTO {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;

    @NotNull
    private Date dob;

    private Long presentAge;
}
