package com.age.agebackend.dto;

import lombok.Data;
import java.util.Date;

@Data
public class AgeResDTO {

    private String firstName;

    private String lastName;

    private Date dob;

    private Long presentAge;


}
