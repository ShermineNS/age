package com.age.agebackend.utils;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AppResponse<T> {

    private T data;

    private boolean success;

    private String message;

    public AppResponse(final T datum, final boolean status,
                       final String msg) {
        this.data = datum;
        this.success = status;
        this.message = msg;
    }
}
