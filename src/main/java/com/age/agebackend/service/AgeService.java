package com.age.agebackend.service;

import com.age.agebackend.dto.AgeReqDTO;
import com.age.agebackend.dto.AgeResDTO;

public interface AgeService {

   AgeResDTO saveAge(AgeReqDTO ageReqDTO);

   }
