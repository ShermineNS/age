package com.age.agebackend.repository;


import com.age.agebackend.entity.Age;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AgeRepository extends JpaRepository<Age,Long> {

}
