CREATE TABLE `age`.`calculation`(
  `id` BIGINT(20) NOT NULL,
  `first_name` VARCHAR(25),
  `last_name` VARCHAR(25),
  `dob` DATE,
  `present_age` TINYINT(3),
  PRIMARY KEY (`id`)
);
